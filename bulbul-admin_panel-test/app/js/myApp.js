var myApp = angular.module('myAppName', ['angle', 'uiGmapgoogle-maps']);

myApp.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
    GoogleMapApi.configure({
//    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}]);
myApp.run(["$log", function ($log) {

    //$log.log('I\'m a line from custom.js');

}]);

App.constant("MY_CONSTANT", {
    "url": "http://54.193.37.203:2500",
    "HAIR": "Hair",
    "MAKEUP": "Make-up",
    "HAIRMAKEUP": "Hair, Make-up",
    "HAIRVALUE": "1",
    "MAKEUPVALUE": "2",   //"HAIRMAKEUPVALUE": "3",
    "WAX": "Wax",
    "REJUVENATE": "Rejuvenate",
    "WAXVALUE":"3",
    "REJUVENATEVALUE":"4",
    "HAIRM":"Hair_Male",
    "GROOMING":"Grooming",
    "FACE":"Face",
    "PAMPERM":"Pamper_Male",
    "HAIRMVALUE":"5",
    "GROOMINGVALUE":"6",
    "FACEVALUE":"7",
    "PAMPERMVALUE":"8"
});

App.constant("responseCode", {
    "SUCCESS": 200
});
myApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper) {
        'use strict';

        //Stripe.setPublishableKey('pk_test_uwFTQNMZNR97NNkYRR9lfnKU');

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);

        // default route
        $urlRouterProvider.otherwise('/page/login');

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
            //
            // Single Page Routes
            // -----------------------------------
            .state('page', {
                url: '/page',
                templateUrl: 'app/pages/page.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley'),
                controller: ["$rootScope", function ($rootScope) {
                    $rootScope.app.layout.isBoxed = false;
                }]
            })
            .state('page.login', {
                url: '/login',
                title: "Login",
                templateUrl: 'app/pages/login.html'
            })
            .state('page.register', {
                url: '/register',
                title: "Register",
                templateUrl: 'app/pages/register.html'
            })
            .state('page.recover', {
                url: '/recover',
                title: "Recover",
                templateUrl: 'app/pages/recover.html'
            })
            .state('page.terms', {
                url: '/terms',
                title: "Terms & Conditions",
                templateUrl: 'app/pages/terms.html'
            })
            .state('page.404', {
                url: '/404',
                title: "Not Found",
                templateUrl: 'app/pages/404.html'
            })

            //App routes
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: helper.basepath('app.html'),
                controller: 'AppController',
                resolve: helper.resolveFor('modernizr', 'icons', 'screenfull', 'whirl', 'ngDialog')
            })
            .state('app.dashboard', {
                url: '/dashboard',
                title: 'Dashboard',
                templateUrl: helper.basepath('dashboard.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.customers', {
                url: '/customers/:id',
                title: 'Customer Information',
                templateUrl: helper.basepath('customer_list.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.customerInfo', {
                url: "/customerinfo/{id:[0-9]*}",
                title: 'Customer Details',
                templateUrl: helper.basepath('customer-info.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.verified-artist', {
                url: '/verified-artist',
                title: 'Verified Artists',
                templateUrl: helper.basepath('verified_artist.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.artistInfo', {
                url: "/artistinfo/{id:[0-9]*}",
                title: 'Artist Details',
                templateUrl: helper.basepath('artist-info.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.requested-artist', {
                url: '/requested-artist',
                title: 'New Requested Artists',
                templateUrl: helper.basepath('new_requested_artist.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.ongoing', {
                url: '/ongoing',
                title: 'Ongoing Session',
                templateUrl: helper.basepath('ongoing.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.upcoming', {
                url: '/upcoming',
                title: 'Upcoming Session',
                templateUrl: helper.basepath('upcoming.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.rejected', {
                url: '/rejected',
                title: 'Rejected Session',
                templateUrl: helper.basepath('rejected.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.past', {
                url: '/past',
                title: 'Past Session',
                templateUrl: helper.basepath('past.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.service', {
                url: '/service',
                title: 'Services Information',
                templateUrl: helper.basepath('service.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngWig', 'parsley')
            })
            .state('app.maleService', {
                url: '/maleService',
                title: 'maleService Information',
                templateUrl: helper.basepath('maleService.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngWig', 'parsley')
            })
            .state('app.driverdatabase', {
                url: '/driverdatabase',
                title: 'Driver Database',
                templateUrl: helper.basepath('driver_database.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.payment', {
                url: '/payment',
                title: 'Payment',
                templateUrl: helper.basepath('payment.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.paymentInfo', {
                url: "/{id:[0-9]*}",
                title: 'Payment Details',
                templateUrl: helper.basepath('payment-info.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.activepromo', {
                url: '/promo',
                title: 'Promo Codes',
                templateUrl: helper.basepath('active_promo.html'),
                resolve: helper.resolveFor('parsley', 'datatables', 'datatables-pugins')
            })

            .state('app.expiredpromo', {
                url: '/expiredpromo',
                title: 'Expired Promo',
                templateUrl: helper.basepath('expired_promo.html'),
                resolve: helper.resolveFor('parsley', 'datatables', 'datatables-pugins')
            })
            .state('app.offerPromoCode', {
                url: '/offerPromoCode',
                title: 'Offer Promo Code',
                templateUrl: helper.basepath('offer_promo_code.html'),
                resolve: helper.resolveFor('parsley', 'datatables', 'datatables-pugins')
            })

            .state('app.userfeedback', {
                url: '/feedback',
                title: 'Feedback',
                templateUrl: helper.basepath('feedback.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.artistfeedback', {
                url: '/artistfeedback',
                title: 'Artist Feedback',
                templateUrl: helper.basepath('artist_feedback.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.reports', {
                url: '/reports',
                title: 'Reports',
                templateUrl: helper.basepath('reports.html')
            })
            .state('app.pushNotification', {
                url: '/pushNotification',
                title: 'Push Notification',
                templateUrl: helper.basepath('send_push_notification.html')
            })
            .state('app.todayBooking', {
                url: '/todayBooking',
                title: 'Today Booking',
                templateUrl: helper.basepath('today_booking.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })
            .state('app.cartDetails', {
            url: "/cartDetails/{id:[0-9]*}",
            title: 'cart Details',
            templateUrl: helper.basepath('cartDetails.html'),
            resolve: helper.resolveFor('datatables', 'datatables-pugins')
            })



        //
        // CUSTOM RESOLVES
        //   Add your own resolves properties
        //   following this object extend
        //   method
        // -----------------------------------
        // .state('app.someroute', {
        //   url: '/some_url',
        //   templateUrl: 'path_to_template.html',
        //   controller: 'someController',
        //   resolve: angular.extend(
        //     helper.resolveFor(), {
        //     // YOUR RESOLVES GO HERE
        //     }
        //   )
        // })

        /**=========================================================
         * Conversion of Date & Time Format common factory
         =========================================================*/
        App.factory('convertdatetime', function () {
            return {

                convertDate: function (DateTime) {
                    var _utc = new Date(DateTime);
                    console.log(_utc);
                  /*  if (_utc.getUTCMonth().toString().length == 1) {
                        var month = "0" + (parseInt(_utc.getUTCMonth()) + 1);
                    } else {
                        month = parseInt(_utc.getUTCMonth()) + 1;
                    }
                    if (_utc.getUTCDate().toString().length == 1) {
                        var day = "0" + (parseInt(_utc.getUTCDate()) + 1);
                    } else {
                        day = parseInt(_utc.getUTCDate()) + 1;
                    }*/
                    //var _utc = _utc.getUTCFullYear() + "-" + _utc.getUTCMonth() + "-" + _utc.getUTCDay();

                    console.log(_utc);
                    return _utc;
                },
                convertDateTime: function (DateTime) {
                    console.log(DateTime);
                    var actualEndTimeDate = DateTime.split("T")[0];
                    var actualEndTimeTime = DateTime.split("T")[1];
                    var actualEndTimeDateString = actualEndTimeDate.split("-");
                    var finalEndDate = actualEndTimeDateString[1] + "/" + actualEndTimeDateString[2] + "/" + actualEndTimeDateString[0];
                    var actualEndTimeTimeString = actualEndTimeTime.split(".")[0];
                    actualEndTimeTimeString = actualEndTimeTimeString.split(":");
                    var actualEndsuffix = actualEndTimeTimeString[0] >= 12 ? "PM" : "AM",
                        actualEndhours12 = actualEndTimeTimeString[0] % 12;
                    var actualEnddisplayTime1 = actualEndhours12 + ":" + actualEndTimeTimeString[1] + " " + actualEndsuffix;
                    var actualEnddisplayTime = finalEndDate + " " + actualEnddisplayTime1;
                    console.log(actualEnddisplayTime);
                    return actualEnddisplayTime;
                }
            };
        })

        /**=========================================================
         * filter phone no in indian format
         =========================================================*/

        App.factory('convertMobileNo', function () {
            return {
                convertPhoneNo: function(mobile){
                    if(mobile=" "){
                       return mobile;
                    }
                    else {
                        var to = "";
                        mobile = mobile.substr(1); //console.log(mobile);
                        var m = mobile.split(")");
                        //var to=m[0].substr(0,2);
                        to += m[0].substr(3, 5);
                        var temp = m[1].split("-");
                        temp[0] = temp[0].substr(1);
                        to += temp[0] + temp[1];
                        return to;
                    }
                }
            }
        })

        /**=========================================================
         * Provides a simple demo for bootstrap datepicker
         =========================================================*/

        App.controller('DatepickerDemoCtrl', ['$scope', function ($scope) {
            $scope.today = function () {
                $scope.dt = new Date();
            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            // Disable weekend selection
            $scope.disabled = function (date, mode) {
                //return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            };

            $scope.toggleMin = function () {
                $scope.minDate = new Date();
            };
            $scope.toggleMin();

            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.initDate = new Date('2016-15-20');
            $scope.formats = ['yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];

        }]);

        /**=============================================================
         * Provides a simple demo for bootstrap datepicker for reports
         =============================================================*/

        App.controller('DatepickerReportsCtrl', ['$scope', function ($scope) {
            $scope.today = function () {
                $scope.dt = new Date();
            };
            $scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            // Disable weekend selection
            $scope.disabled = function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            };

            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.initDate = new Date('2016-15-20');
            $scope.formats = ['yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];

        }]);

        /**=========================================================
         * Provides a simple demo for buttons actions
         =========================================================*/

        App.controller('ButtonsCtrl', ['$scope', function ($scope) {
            console.log($scope);
            $scope.radioModel = 'Left';

        }]);

        /**=========================================================
         * Initializes the masked inputs
         =========================================================*/

        App.directive('masked', function () {
            return {
                restrict: 'A',
                controller: ["$scope", "$element", function ($scope, $element) {
                    var $elem = $($element);
                    if ($.fn.inputmask)
                        $elem.inputmask();
                }]
            };
        });

    }]);
/**=========================================================
 * filter phone no in indian format
 =========================================================*/

App.factory('checkServiceName', function (MY_CONSTANT) {
    return {
        serviceName: function(value){
            if(value==1){
             return "Blow Dry,";
            }
            if(value==2){
                return "Face Bleach,";
            }
            if(value==3){
                return "Straightening,";
            }
            if(value==4){
                return "Hair Do,";
            }
            if(value==5){
                return "Face+Neck Bleach,";
            }
            if(value==6){
                return "Clean Up,";
            }
            if(value==7){
                return "Whitening Facial,";
            }
            if(value==8){
                return "Anti Aging Facial,";
            }
            if(value==9){
                return "Arms";
            }
            if(value==10){
                return "Legs";
            }
            if(value==11){
                return "Arms+Legs Waxing";
            }
            if(value==13){
                return "Manicure";
            }
            if(value==14){
                return "Pedicure";
            }
            if(value==15){
                return "Manicure & Pedicure";
            }
            if(value==50){
                return "Touch Up";
            }
            if(value==16){
                return "Head Massage";
            }
            if(value==46){
                return "Body Massage";
            }
            if(value==12){
                return "Full Wax";
            }
            if(value==29){
                return "Root TouchUp";
            }
            if(value==32){
                return "Bikini Waxing";
            }
            if(value==33){
                return "Full Body Waxing";
            }
            if(value==39){
                return "Foot Massage";
            }
            if(value==40){
                return "Light MakeUp";
            }
            if(value==42){
                return "Back Bleach";
            }
            if(value==43){
                return "Full Body Bleach";
            }
            if(value==44){
                return "Body Polishing";
            }


        }
    }
})