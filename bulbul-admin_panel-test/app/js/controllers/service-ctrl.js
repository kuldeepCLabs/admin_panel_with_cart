App.controller('ServiceController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, $route, ngDialog, $window) {
    'use strict';
    $scope.loading = true;

    var getServiceList = function () {
        $.post(MY_CONSTANT.url + '/services', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data.forEach(function (column) {

                var d = {
                    service_id: "",
                    category: "",
                    name: "",
                    image_url: "",
                    heading: "",
                    description: "",
                    instructions: "",
                    time: "",
                    cost: ""
                };
                if(column.user_status==2) {
                    d.service_id = column.service_id;
                    d.category = column.category;
                    d.name = column.name;
                    d.image_url = column.image_url;
                   // d.heading = column.heading;
                    d.description = column.description;
                   // d.instructions = column.instructions;
                    d.time = column.time;
                    d.cost = column.cost;
                    dataArray.push(d);
                }

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getServiceList();

    // Delete Dialog
    $scope.deleteService = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_service',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                service_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };


    // Add Driver Dialog
    $scope.addServiceDialog = function (flag) {
        $scope.value = true;
        $scope.content = '';
        $scope.addTeam = {};
        $scope.addService = {};
        $scope.addService.desc_title = 'b.';
        $scope.flag=flag;

        /*$state.go('app.add_menu1',{s_date:$scope.startDate1,e_date:$scope.endDate1,s_time:$scope.startTime,e_time:$scope.endTime});*/
        ngDialog.open({
            template: 'app/views/add-service-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    // Edit Driver Dialog
    $scope.editService = {};
    $scope.editServiceDialog = function (json) {

        $scope.options = [{
            name: MY_CONSTANT.HAIR,
            value: MY_CONSTANT.HAIRVALUE
        }, {
            name: MY_CONSTANT.MAKEUP,
            value: MY_CONSTANT.MAKEUPVALUE
        }, {
            name: MY_CONSTANT.WAX,
            value: MY_CONSTANT.WAXVALUE
        }, {
            name: MY_CONSTANT.REJUVENATE,
            value: MY_CONSTANT.REJUVENATEVALUE
        }];


        $scope.editService.id_pop = json.service_id;
        //$scope.editService.category = [{value: json.category}];
        $scope.updateVal = json.category;
        $scope.editService.servicename = json.name;
        $scope.editService.desc_title = json.heading;
        $scope.editService.description = json.description;
        $scope.editService.cost = json.cost;
        $scope.editService.servicetime = json.time;
        $scope.content = json.instructions;

        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/edit-service-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

});


App.controller('MaleServiceController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, $route, ngDialog, $window) {
    'use strict';
    $scope.loading = true;

    var getServiceList = function () {
        $.post(MY_CONSTANT.url + '/services', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data.forEach(function (column) {

                var d = {
                    service_id: "",
                    category: "",
                    name: "",
                    image_url: "",
                    heading: "",
                    description: "",
                    instructions: "",
                    time: "",
                    cost: ""
                };
                if(column.user_status==1) {
                    d.service_id = column.service_id;
                    d.category = column.category;
                    d.name = column.name;
                    d.image_url = column.image_url;
                    // d.heading = column.heading;
                    d.description = column.description;
                    // d.instructions = column.instructions;
                    d.time = column.time;
                    d.cost = column.cost;
                    dataArray.push(d);
                }

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getServiceList();

    // Delete Dialog
    $scope.deleteService = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_service',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                service_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };


    // Add Driver Dialog
    $scope.addServiceDialog = function (flag) {
        $scope.value = true;
        $scope.content = '';
        $scope.addTeam = {};
        $scope.addService = {};
        $scope.addService.desc_title = 'b.';
        $scope.flag=flag;

        ngDialog.open({
            template: 'app/views/add-service-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    // Edit Driver Dialog
    $scope.editService = {};
    $scope.editServiceDialog = function (json) {
        console.log("print the json in ");
          console.log(json);
     /*   $scope.options=[{
            name: MY_CONSTANT.HAIRM,
            value: MY_CONSTANT.HAIRMVALUE
        }, {
            name: MY_CONSTANT.GROOMING,
            value: MY_CONSTANT.GROOMINGVALUE
        }, {
            name: MY_CONSTANT.FACE,
            value: MY_CONSTANT.FACEVALUE
        }, {
            name: MY_CONSTANT.PAMPERM,
            value: MY_CONSTANT.PAMPERMVALUE
        }];
*/

        $scope.editService.id_pop = json.service_id;
        $scope.editService.category = json.category;
        $scope.updateVal = json.category;
        $scope.editService.servicename = json.name;
        $scope.editService.description = json.description;
        $scope.editService.cost = json.cost;
        $scope.editService.servicetime = json.time;
        $scope.image_url = json.image_url;
        $scope.FileUploaded = json.image_url;

        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/edit-service-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

});


App.controller('AddServiceController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $window) {
    'use strict';
    console.log("get the flag"+$scope.flag);
    $scope.addService.category=1;

    if($scope.flag==2){
        $scope.options = [{
            name: MY_CONSTANT.HAIR,
            value: MY_CONSTANT.HAIRVALUE
        }, {
            name: MY_CONSTANT.MAKEUP,
            value: MY_CONSTANT.MAKEUPVALUE
        }, {
            name: MY_CONSTANT.WAX,
            value: MY_CONSTANT.WAXVALUE
        }, {
            name: MY_CONSTANT.REJUVENATE,
            value: MY_CONSTANT.REJUVENATEVALUE
        }];
    }
    else if($scope.flag==1){
        $scope.options=[{
            name: MY_CONSTANT.HAIRM,
            value: MY_CONSTANT.HAIRMVALUE
        }, {
            name: MY_CONSTANT.GROOMING,
            value: MY_CONSTANT.GROOMINGVALUE
        }, {
            name: MY_CONSTANT.FACE,
            value: MY_CONSTANT.FACEVALUE
        }, {
            name: MY_CONSTANT.PAMPERM,
            value: MY_CONSTANT.PAMPERMVALUE
        }];
    }
    $scope.image1={};
    $scope.file_to_upload = function (File) {
        $scope.FileUploaded = File[0];
        $scope.image1 = File[0];
    };
    //formData.append("csv", $scope.add.csv);
    $scope.successMsg = '';
    $scope.errorMsg = '';
    $scope.AddService = function () {   // flag value decide the 2 male services and 1 for female services
        console.log($scope.image1);


        var cat = $scope.addService.category.value;

        if (cat == MY_CONSTANT.HAIR) {
            cat = MY_CONSTANT.HAIRVALUE;
        }
        if (cat == MY_CONSTANT.MAKEUP) {
            cat = MY_CONSTANT.MAKEUPVALUE;
        }
        if(cat == MY_CONSTANT.WAX){
            cat = MY_CONSTANT.WAXVALUE;
        }
        if(cat == MY_CONSTANT.REJUVENATE){
            cat = MY_CONSTANT.REJUVENATEVALUE;
        }
        if (cat == MY_CONSTANT.HAIRM) {
            cat = MY_CONSTANT.HAIRMVALUE;
        }
        if (cat == MY_CONSTANT.GROOMING) {
            cat = MY_CONSTANT.GROOMINGVALUE;
        }
        if (cat == MY_CONSTANT.FACE){
            cat = MY_CONSTANT.FACEVALUE;
        }
        if (cat == MY_CONSTANT.PAMPERM){
            cat = MY_CONSTANT.PAMPERMVALUE;
        }

        if($scope.addService.description==undefined){
            $scope.addService.description="";
        }

        var formData = new FormData();

        formData.append('access_token', $cookieStore.get('obj').accesstoken);
        formData.append('category', $scope.addService.category.value);
        formData.append('service_name', $scope.addService.servicename);
        formData.append('description', $scope.addService.description);
        formData.append('cost', $scope.addService.cost);
        formData.append('time',  $scope.addService.servicetime);
        formData.append('service_image', $scope.image1);
        formData.append('user_status',$scope.flag);
        console.log(formData);
        $.ajax({
            type: "POST",
            url: MY_CONSTANT.url + '/add_new_service',
            dataType: "json",
            data: formData,

            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                $scope.data = data;
                if(data.error)
                {
                    $scope.errorMsg = data.error;
                }
                if (data.log) {
                    $scope.successMsg = data.log;
                }
                if (data.log) {
                    $scope.closeThisDialog(0);
                    $window.location.reload();
                }
            }


        });
       /* $.post(MY_CONSTANT.url + '/add_new_service',
            {

                access_token: $cookieStore.get('obj').accesstoken,
                category: $scope.addService.category.value,
                service_name: $scope.addService.servicename,
                time: $scope.addService.servicetime,
                cost: $scope.addService.cost,
                description: $scope.addService.description
            }
            ,

            function (data) {
                data = JSON.parse(data);

                if (data.log) {
                    $scope.successMsg = data.log;
                }
                else {
                    $scope.errorMsg = data.error;
                }
                $scope.$apply();
                if (data.log) {
                    $scope.closeThisDialog(0);
                    $window.location.reload();
                }

            });*/
    }
});

App.controller('EditServiceController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $window) {
    'use strict';

    $scope.successMsg = '';
    $scope.errorMsg = '';
    $scope.addService = {};
    $scope.image = {};


    $scope.file_to_upload = function (File) {
        $scope.FileUploaded = File[0];
        $scope.image2 = File[0];
    };



    $scope.EditService = function (val) {

        console.log($scope.image);
            console.log(val);


        var bool;
        if ($scope.image2 != null) {
            bool = 1;
          var file = $scope.image2[0];
        } else {
            bool = 0;
        }

        var formData = new FormData();

        formData.append('access_token', $cookieStore.get('obj').accesstoken);
        formData.append('category', val);
        formData.append('service_id', $scope.editService.id_pop);
        formData.append('service_name', $scope.editService.servicename);
        formData.append('description', $scope.editService.description);
        formData.append('cost', $scope.editService.cost);
        formData.append('time', $scope.editService.servicetime);
        formData.append('service_image', file);
        formData.append('image_sent', bool);
        console.log(formData);
        $.ajax({
            type: "POST",
            url: MY_CONSTANT.url + '/edit_service',
            dataType: "json",
            data: formData,

            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                $scope.data = data;
                if(data.error)
                {
                    $scope.errorMsg = data.error;
                }
                if (data.log) {
                    $scope.successMsg = data.log;
                    $scope.closeThisDialog(0);
                    $window.location.reload();
                }
                $scope.$apply();
            }


        });


        /*$.post(MY_CONSTANT.url + '/edit_service',
            {

                access_token: $cookieStore.get('obj').accesstoken,
                service_id: $scope.editService.id_pop,
                category: val,
                service_name: $scope.editService.servicename,
                time: $scope.editService.servicetime,
                cost: $scope.editService.cost,
                description: $scope.editService.description
            }
            ,

            function (data) {
                data = JSON.parse(data);

                if (data.log) {
                    $scope.successMsg = data.log;
                    $scope.closeThisDialog(0);
                    $window.location.reload();
                }
                else {
                    $scope.errorMsg = data.error;
                }
                $scope.$apply();

            });*/
    }
});

