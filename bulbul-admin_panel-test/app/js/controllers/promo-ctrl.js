App.controller('ActivePromoController', function ($scope, $http, $route, $cookieStore, $timeout, $location, MY_CONSTANT, convertdatetime, $window, ngDialog) {

    var getPromo = function () {
        $.post(MY_CONSTANT.url + '/promotion_code_list', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data = data.active_codes;
            data.forEach(function (column) {

                var d = {
                    code_id: "",
                    user_id: "",
                    code: "",
                    credits: "",
                    create_date: "",
                    start_date: "",
                    expiry_date: ""
                };

                d.user_id = column.user_id;
                d.code_id = column.code_id;
                d.code = column.code;
                d.credits = column.credits;
                var create_date = column.created_time.toString().split("T")[0];
                d.create_date = create_date;
                var start_date = column.start_date.toString().split("T")[0];
                d.start_date = start_date;
                var expiry_date = column.expiry_date.toString().split("T")[0];
                d.expiry_date = expiry_date;
                dataArray.push(d);

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getPromo();

    $scope.promo = {};
    $scope.checkDate = function () {
        if ($scope.promo.end_date < $scope.promo.start_date) {
            ngDialog.open({
                template: '<p class="del-dialog">End date must be greater than start date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
    }

    $scope.addPromo = function () {


        if ($scope.promo.start_date == undefined) {
            ngDialog.open({
                template: '<p class="del-dialog">Please select start date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        else if ($scope.promo.end_date == undefined) {
            ngDialog.open({
                template: '<p class="del-dialog">Please select end date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        else if ($scope.promo.credits == undefined) {
            ngDialog.open({
                template: '<p class="del-dialog">Please fill credits first !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }
        if ($scope.promo.end_date < $scope.promo.start_date) {
            ngDialog.open({
                template: '<p class="del-dialog">End date must be greater than start date !!</p>',
                plain: true,
                className: 'ngdialog-theme-default'

            });
            return false;
        }

        var start_date = convertdatetime.convertDate($scope.promo.start_date);
        var to_date = convertdatetime.convertDate($scope.promo.end_date);

        $.post(MY_CONSTANT.url + '/add_promo_code',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                credits: $scope.promo.credits,
                start_date: start_date,
                expiry_date: to_date
            },
            function (data) {

                data = JSON.parse(data);
                console.log(data);
                if (data.log) {
                    alert(data.log);
                    $window.location.reload();
                }
                else {
                    alert("Promo code can't be added right now !!");
                }
            });


    };

    // Delete Dialog
    $scope.deletePromo = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_promotion_code',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                code_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };

});

App.controller('ExpiredPromoController', function ($scope, $http, $route, $cookieStore, $timeout, $location, MY_CONSTANT, $window, ngDialog) {
    var getPromo = function () {
        $.post(MY_CONSTANT.url + '/promotion_code_list', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            data = data.expired_codes;
            data.forEach(function (column) {

                var d = {
                    code_id: "",
                    user_id: "",
                    code: "",
                    credits: "",
                    create_date: "",
                    start_date: "",
                    expiry_date: ""
                };

                d.user_id = column.user_id;
                d.code_id = column.code_id;
                d.code = column.code;
                d.credits = column.credits;
                var create_date = column.created_time.toString().split("T")[0];
                d.create_date = create_date;
                var start_date = column.start_date.toString().split("T")[0];
                d.start_date = start_date;
                var expiry_date = column.expiry_date.toString().split("T")[0];
                d.expiry_date = expiry_date;
                dataArray.push(d);

            });

            $scope.$apply(function () {
                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    getPromo();

    // Delete Dialog
    $scope.deletePromo = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_promotion_code',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                code_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };

});

App.controller('ItemController', ['$scope', function ($scope) {

    $scope.$parent.isopen = ($scope.$parent.default === $scope.item);

    $scope.$watch('isopen', function (newvalue, oldvalue, scope) {
        $scope.$parent.isopen = newvalue;
    });

}]);

App.controller('SpecialOfferPromoCode', function ($scope, $http, $route, $cookieStore, $timeout, $location, MY_CONSTANT, convertdatetime, $window, ngDialog,$filter,checkServiceName) {

     getAllService();

    $scope.add = {};
    $scope.promo = {};
    $scope.add.offerPromoCode=1;
    $scope.singleServices=true;
    $scope.multipleServices=true;
    $scope.creditsCheck=true;
    $scope.serviceId = true;
    $scope.comboId = true;
    $scope.minDate=new Date();
    $scope.minDate1= new Date();

    $scope.promo.start_date = $filter('date')(new Date(),'yyyy-MM-dd');
    $scope.promo.end_date = $filter('date')(new Date(),'yyyy-MM-dd');
    //console.log("start and end date");
    //console.log($scope.promo.start_date,$scope.promo.end_date);
    console.log($scope.add.offerPromoCode);

    $scope.startDate = function(){
        $scope.promo.start_date = $filter('date')($scope.promo.start_date,'yyyy-MM-dd');
    }
    $scope.endDate = function(){
        $scope.promo.end_date = $filter('date')($scope.promo.end_date,'yyyy-MM-dd');
    }

   /* $scope.list1 = [
        {
            "id": +MY_CONSTANT.BLOWDRY+"", "data": "0", "service_name":'Blow Dry'
        },
        {
            "id": +MY_CONSTANT.STRAIGHTENING+"", "data": "0", "service_name":'Straightening'
        },
        {
            "id":  +MY_CONSTANT.HAIRDO+"", "data": "0", "service_name":'Hair Do'
        },
        {
            "id":  +MY_CONSTANT.ROOTTOUCHUP+"", "data": "0", "service_name":'Root Touch Up'
        },
        {
            "id":  +MY_CONSTANT.TOUCHUP+"", "data": "0", "service_name":'Touch Up'
        },
        {
            "id":  +MY_CONSTANT.FACEBLEACH+"", "data": "0", "service_name":'Face Bleach'
        },
        {
            "id": +MY_CONSTANT.FACENECKBLEACH+"", "data": "0", "service_name":'Face+Neck Bleach'
        },
        {
            "id":  +MY_CONSTANT.CLEANUP+"", "data": "0", "service_name":'Clean Up'
        },
        {
            "id":  +MY_CONSTANT.WHITENINGFACIAL+"", "data": "0", "service_name":'Whitening Facial'
        },
        {
            "id":  +MY_CONSTANT.ANTIAGINGFACIAL+"", "data": "0", "service_name":'Anti Aging Facial'
        },
        {
            "id":  +MY_CONSTANT.LIGHTMAKEUP+"", "data": "0", "service_name":'Light Make Up'
        },
        {
            "id":  +MY_CONSTANT.ARMS+"", "data": "0", "service_name":'Arms Waxing'
        },
        {
            "id":  +MY_CONSTANT.LEGS+"", "data": "0", "service_name":'Legs Waxing'
        },
        {
            "id": +MY_CONSTANT.FULLWAX+"", "data": "0", "service_name":'Arms+Legs Waxing'
        },
        {
            "id":  +MY_CONSTANT.BIKINIWAX+"", "data": "0", "service_name":'Bikini Waxing'
        },
        {
            "id":  +MY_CONSTANT.FULLBODY+"", "data": "0", "service_name":'Full Body Waxing'
        },
        {
            "id":  +MY_CONSTANT.BACKBLEACH+"", "data": "0", "service_name":'Back Bleach'
        },
        {
            "id":  +MY_CONSTANT.FULLBODYBLEACH+"", "data": "0", "service_name":'Full Body Bleach'
        },
        {
            "id":  +MY_CONSTANT.BODYPOLISHING+"", "data": "0", "service_name":'Body Polishing'
        },
        {
            "id": MY_CONSTANT.MANICURE, "data": "0", "service_name":'Manicure'
        },
        {
            "id":  +MY_CONSTANT.PEDICURE+"", "data": "0", "service_name":'Pedicure'
        },
        {
            "id": +MY_CONSTANT.MANIANDPEDI+"", "data": "0", "service_name":'Manicure & Pedicure'
        },
        {
            "id":  +MY_CONSTANT.HEADMASSAGE+"", "data": "0", "service_name":'Head Massage'
        },
        {
            "id":  +MY_CONSTANT.FOOTMASSAGE+"", "data": "0", "service_name":'Foot Massage'
        },
        {
            "id":  +MY_CONSTANT.BODYMASSAGE+"", "data": "0", "service_name":'Body Massage'
        }
    ];*/

    $scope.list2 = $scope.list1;


    $scope.offer_promo_code=function(){

        if($scope.add.offerPromoCode=="1"){
           // $scope.singleServices=true;
           // $scope.multipleServices=true;
            $scope.creditsCheck=true;
            $scope.discountCheck=false;
            $scope.serviceId = true;
            $scope.comboId = true;
            $scope.serviceName = "";
            getPromo(1);
        }
        if($scope.add.offerPromoCode=="2"){
           // $scope.singleServices=true;
           // $scope.multipleServices=true;
            $scope.discountCheck=false;
            $scope.creditsCheck=false;
            $scope.serviceId = true;
            $scope.comboId = true;
            $scope.serviceName = "";
            getPromo(2);
        }
        if($scope.add.offerPromoCode=="3"){
            $scope.serviceId=false;
            $scope.comboId = true;
            getPromo(3);
         //   $scope.singleServices=false;
           // $scope.multipleServices=true;
            $scope.discountCheck=true;
            $scope.creditsCheck=false;
            $scope.serviceName = "";
            if($scope.serviceType){
                $scope.serviceType = !$scope.serviceType;
            }
        }
        if($scope.add.offerPromoCode=="4"){
            $scope.serviceId=false;
            $scope.comboId = true;
            getPromo(4);
           // $scope.singleServices=false;
           // $scope.multipleServices=true;
            $scope.discountCheck=false;
            $scope.creditsCheck=false;
            $scope.serviceName = "";
            if($scope.serviceType){
                $scope.serviceType = !$scope.serviceType;
            }
        }
        if($scope.add.offerPromoCode=="5"){

          //  $scope.singleServices=true;
           // $scope.multipleServices=false;
            $scope.discountCheck=true;
            $scope.creditsCheck=false;
            $scope.serviceId=true;
            $scope.comboId = false;
            $scope.serviceName = "";
            getPromo(5);
        }
        if($scope.add.offerPromoCode=="6"){
          //  $scope.singleServices=true;
          //  $scope.multipleServices=false;
            $scope.discountCheck=false;
            $scope.creditsCheck=false;
            $scope.serviceId=true;
            $scope.comboId = false;
            $scope.serviceName = "";
            getPromo(6);
        }
    }

    $scope.setChoiceForService =function(arr,choice){
        $scope.serviceType = choice.id;
        $scope.serviceName = choice.service_name;
       /* angular.forEach(arr, function (choice) {
            choice.data = 0;
        });

        choice.data = 1;*/
    }



    $scope.addPromo=function(){

        console.log($scope.serviceType);
        console.log($scope.serviceName);
        var flag=$scope.add.offerPromoCode;
        var comboServicesArray=[];

        for(var i=0;i<$scope.list1.length;i++){
            if($scope.list1[i].data==1){
                console.log($scope.list1[i].id);
                comboServicesArray.push($scope.list1[i].id);
                $scope.serviceName += $scope.list1[i].service_name+"  ";
            }
        }
        console.log($scope.serviceName);
        if ($scope.promo.start_date == undefined) {
            openDialogBox("Please select start date !!");
            return false;
        }
        else if ($scope.promo.end_date == undefined) {
            openDialogBox("Please select end date !!");
            return false;
        }
        if($scope.code == undefined){
            openDialogBox("Please fill Promo Code !!");
            return false;
        }
       /* console.log($scope.promo.end_date<$scope.promo.start_date);
        console.log($scope.promo.end_date,$scope.promo.start_date);*/
        if ($scope.promo.end_date < $scope.promo.start_date) {
            openDialogBox("End date must be greater than start date !!");
            return false;
        }
        if(flag==1){
             if ($scope.discount == undefined) {
                 openDialogBox("Please fill discount first !!");
                 return false;
            }
        }
        if(flag==2){
            if ($scope.discount == undefined) {
                openDialogBox("Please fill discount first !!");
                return false;
            }
        }
        if(flag==3){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if ($scope.serviceType == undefined) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }
        if(flag==4){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if ($scope.discount == undefined) {
                openDialogBox("Please fill discount first !!");
                return false;
            }
            if ($scope.serviceType == undefined) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }
        if(flag==5){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if (comboServicesArray== undefined||comboServicesArray.length==0) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }
        if(flag==6){
            if ($scope.credits == undefined) {
                openDialogBox("Please fill credits first !!");
                return false;
            }
            if ($scope.discount == undefined) {
                openDialogBox("Please fill discount first !!");
                return false;
            }
            if (comboServicesArray== undefined||comboServicesArray.length==0) {
                openDialogBox("Please select the service option first !!");
                return false;
            }
        }

       // console.log($scope.start_date,$scope.end_date,$scope.discount,$scope.serviceType);
        $.post(MY_CONSTANT.url + '/add_special_offers', {
            access_token: $cookieStore.get('obj').accesstoken,
            start_date:$scope.promo.start_date,
            expiry_date:$scope.promo.end_date,
            flag:$scope.add.offerPromoCode,
            price:$scope.credits,
            discount:$scope.discount,
            code:$scope.code,
            service_type:$scope.serviceType,
            combo_services:comboServicesArray,
            service_name:$scope.serviceName

        }, function (data) {
            data = JSON.parse(data);
            console.log(data);
            if(data.log){
                ngDialog.open({
                    template: '<p class="del-dialog">'+data.log+'</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
                $timeout(function() {
                    location.reload(true);
                }, 1000);
            }
            else{
                ngDialog.open({
                    template: '<p class="del-dialog">'+data.error+'</p>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });
                return false;
            }
        });

     function  openDialogBox(message){
            ngDialog.open({
                template: '<p class="del-dialog">'+message+'</p>',
                plain: true,
                className: 'ngdialog-theme-default'
            });
        }

    }



    var getPromo = function (flag) {

        $.post(MY_CONSTANT.url + '/list_of_special_offer', {
            access_token: $cookieStore.get('obj').accesstoken,
            flag: flag

        }, function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }

            data.forEach(function (column) {

                var d = {
                    code_id: "",
                    code: "",
                    price: "",
                    create_date: "",
                    start_date: "",
                    expiry_date: "",
                    discount:"",
                    description:""
                  /*  single_service_id:"",
                    combo_service_id:""*/
                };

                d.code_id = column.code_id;
                d.code = column.code;
                d.price = column.service_rate;
                d.discount = column.discount;
                var create_date = column.created_time.toString().split("T")[0];
                d.create_date = create_date;
                var start_date = column.start_date.toString().split("T")[0];
                d.start_date = start_date;
                var expiry_date = column.expiry_date.toString().split("T")[0];
                d.expiry_date = expiry_date;
                d.description = column.description;
              /*  d.single_service_id = checkServiceName.serviceName(column.service_id_promo_code);
                //console.log(column.combo_service_id);
                 var comboId = column.combo_service_id.split(',');
                for(var i=0;i<comboId.length;i++){
                    d.combo_service_id += checkServiceName.serviceName(comboId[i])+"   ";
                }*/
                dataArray.push(d);

            });
            $scope.$apply(function () {

                if ($.fn.DataTable.isDataTable("#datatable2")) {
                    console.log("hello");
                    $('#datatable2').DataTable().clear().destroy();
                }

                $scope.list = dataArray;


                // Define global instance we'll use to destroy later
                var dtInstance;
                $scope.loading = false;
                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        'bDestroy': true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });

        });
    };

    function getAllService(){
        $.post(MY_CONSTANT.url + '/get_all_service', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            data = JSON.parse(data);
            console.log(data);
            if (data.error) {
                ngDialog.open({
                    template: '<p>Something went wrong !</p>',
                    className: 'ngdialog-theme-default',
                    plain: true,
                    showClose: false,
                    closeByDocument: false,
                    closeByEscape: false
                });
                return false;
            }
            else{
                for(var i=0;i<data.length;i++){
                    data[i].data=0;
                }
                console.log(data);
                /*return data;*/
                $scope.list1 = data;
            }
        });
    }

    $scope.deletePromo = function (userid) {
        $scope.dele_val = userid;
        $scope.value = true;
        $scope.addTeam = {};
        ngDialog.open({
            template: 'app/views/delete-dialog.html',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.delete = function () {

        $.post(MY_CONSTANT.url + '/delete_special_offer',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                code_id: $scope.dele_val
            },
            function (data) {
                $window.location.reload();

            });

    };

    getPromo(1);


});