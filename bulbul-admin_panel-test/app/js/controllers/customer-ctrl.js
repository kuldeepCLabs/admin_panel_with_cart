App.controller('CustomersController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, $window,$stateParams, ngDialog,$state) {
    'use strict';
  // $scope.loading = true;

        $scope.city_name=1;


      customerCity($scope.city_name);

      $scope.cityName=function(){
          $scope.city_name = $scope.city_name;
          customerCity($scope.city_name);
      }

      function customerCity(value){

          console.log($scope.city_name);

           var cityName1="";

           if(value==1){
               cityName1 = "Chandigarh";
           }
           if(value==2){
               cityName1 = "Mohali";
           }
           if(value==3){
               cityName1 = "Zirakpur";
           }
           if(value==4){
               cityName1 = "Panchkula";
           }
           if(value==5){
               cityName1 = "Gurgaon";
           }
           if(value==6){
               cityName1 = "South Delhi";
           }
           if(value==7){
               cityName1 = "";
           }

           console.log(cityName1);

           $("#datatable2").dataTable({
               "sPaginationType": "full_numbers",
               "processing": true,
               "bServerSide": true,
               "bSearchable": true,
              // "bRetrieve":true,
               "bDestroy":true,
               "sAjaxSource": 'http://54.193.37.203:2500/customer_list?access_token=' + $cookieStore.get('obj').accesstoken+"&city="+cityName1
           });
       }

         $scope.changeStatus = function(){

             console.log("call change status");
         }
        /* function changeStatus(){
             console.log("heloodfggeterte");
         }
         $('block').click(function(){
             console.log("cjdshjfh8wrwe");
         })*/
          /*  var getValue = $stateParams.id;

            console.log($stateParams.id);*/

            /*var value = getValue.split('_');
            if(value[0]=="delete"){
                deleteCustomer(value[1]);
            }
            if(value[0]=="block"){
                changeStatus(1,value[1]);
            }
            if(value[0]=="unblock"){
                changeStatus(0,value[1]);
            }*/


});

App.controller('CustomerInfoController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, $stateParams, convertdatetime) {
    'use strict';
    $scope.loading = true;
    $scope.loading_image = true;
    $scope.customer = {};
    var userId = $stateParams.id;
    $.post(MY_CONSTANT.url + '/customer_details', {
        access_token: $cookieStore.get('obj').accesstoken,
        user_id: userId

    }, function (data) {
        var dataArray = [];
        data = JSON.parse(data);
        console.log(data);
        if (data.error) {
            ngDialog.open({
                template: '<p>Something went wrong !</p>',
                className: 'ngdialog-theme-default',
                plain: true,
                showClose: false,
                closeByDocument: false,
                closeByEscape: false
            });
            return false;
        }

        var customer_data = data.customer_detail[0];

        $scope.customer.image = customer_data.image_url;
        $scope.customer.name = customer_data.name;
        $scope.customer.email = customer_data.email;
        $scope.customer.phone = customer_data.mobile;
        $scope.loading_image = false;

        data = data.services;
        data.forEach(function (column) {

            var d = {
                id: "",
                booking_id: "",
                service_id: "",
                address: "",
                artist_name: "",
                service_date: "",
                start_time: "",
                end_time: "",
                cancelation_time: "",
                status: "",
                rating: "",
                cost: "",
                payable_amount: "",
                amount_refunded: "",
                category: "",
                service_name: ""
            };

            var date = column.service_date.toString().split("T")[0];
            var startTimeHours = column.start_time.split(":")[0];
            var startTimeMinutes = column.start_time.split(":")[1];
            var startSuffix = startTimeHours >= 12 ? "PM" : "AM",
                starthours12 = startTimeHours % 12;
            var startDisplayTime = starthours12 + ":" + startTimeMinutes + " " + startSuffix;
            //console.log(startDisplayTime);
            var endTimeHours = column.end_time.split(":")[0];
            var endTimeMinutes = column.end_time.split(":")[1];
            var endSuffix = endTimeHours >= 12 ? "PM" : "AM",
                endhours12 = startTimeHours % 12;
            var endDisplayTime = endhours12 + ":" + endTimeMinutes + " " + endSuffix;
            if (column.status == 7 && column.cancelled_at != "" && column.cancelled_at != "0000-00-00 00:00:00") {
                var canceled_at = convertdatetime.convertDateTime(column.cancelled_at);
            }
            else {
                canceled_at = "-";
            }
            d.id = column.id;
            d.booking_id = column.booking_id;
            d.service_id = column.service_id;
            d.status = column.status;
            d.artist_name = column.artist_name;
            d.start_time = startDisplayTime;
            d.end_time = endDisplayTime;
            d.cancelation_time = canceled_at;
            d.address = column.address;
            d.service_date = date;
            d.rating = column.rating;
            d.cost = column.cost;
            d.payable_amount = column.payable_amount;
            d.amount_refunded = column.amount_refunded;
            d.category = column.category;
            d.service_name = column.service_name;
            dataArray.push(d);
        });

        $scope.$apply(function () {
            $scope.list = dataArray;


            // Define global instance we'll use to destroy later
            var dtInstance;
            $scope.loading = false;
            $timeout(function () {
                if (!$.fn.dataTable) return;
                dtInstance = $('#datatable2').dataTable({
                    'paging': true,  // Table pagination
                    'ordering': true,  // Column ordering
                    'info': true,  // Bottom left status text
                    // Text translation options
                    // Note the required keywords between underscores (e.g _MENU_)
                    oLanguage: {
                        sSearch: 'Search all columns:',
                        sLengthMenu: '_MENU_ records per page',
                        info: 'Showing page _PAGE_ of _PAGES_',
                        zeroRecords: 'Nothing found - sorry',
                        infoEmpty: 'No records available',
                        infoFiltered: '(filtered from _MAX_ total records)'
                    }
                });
                var inputSearchClass = 'datatable_input_col_search';
                var columnInputs = $('tfoot .' + inputSearchClass);

                // On input keyup trigger filtering
                columnInputs
                    .keyup(function () {
                        dtInstance.fnFilter(this.value, columnInputs.index(this));
                    });
            });

            // When scope is destroyed we unload all DT instances
            // Also ColVis requires special attention since it attaches
            // elements to body and will not be removed after unload DT
            $scope.$on('$destroy', function () {
                dtInstance.fnDestroy();
                $('[class*=ColVis]').remove();
            });
        });

    });

});