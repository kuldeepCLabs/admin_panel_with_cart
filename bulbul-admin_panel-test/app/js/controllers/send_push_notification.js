/**
 * Created by Chirag on 30/06/15.
 */
App.controller('PushController', function ($scope, $http, $location, $cookies, $cookieStore, MY_CONSTANT, ngDialog, $timeout) {
    'use strict';
   // $scope.loading = true;
   $scope.sendPushNotification=function(){

       if($scope.notification_content==undefined||$scope.notification_content==""){
           ngDialog.open({
               template: '<p class="del-dialog">Plz enter the content for sending the notification !!</p>',
               plain: true,
               className: 'ngdialog-theme-default'

           });
           return false;
       }
       else if($scope.title==undefined||$scope.title==""){
           ngDialog.open({
               template: '<p class="del-dialog">Plz enter the title for the push notification !!</p>',
               plain: true,
               className: 'ngdialog-theme-default'

           });
           return false;
       }
       else {

           $.post(MY_CONSTANT.url + '/send_push_notification', {
               access_token: $cookieStore.get('obj').accesstoken,
               notification_message: $scope.notification_content,
               title: $scope.title

           }, function (data) {
               var dataArray = [];
               data = JSON.parse(data);
               console.log(data);
               if (data.error) {
                   ngDialog.open({
                       template: data.error,
                       plain: true,
                       className: 'ngdialog-theme-default'

                   });
                   return false;
               }
               else {
                   ngDialog.open({
                       template: data.log,
                       plain: true,
                       className: 'ngdialog-theme-default'
                   });
                   $timeout(function() {
                       location.reload(true);
                   }, 3000);
               }
           });
       }
   }
});